---
layout: handbook-page-toc
title: Iterable
description: Iterable Overview
twitter_image: /images/tweets/handbook-marketing.png
twitter_site: '@gitlab'
twitter_creator: '@gitlab'
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# <i class="fab fa-gitlab fa-fw" style="color:rgb(252,109,38); font-size:.85em" aria-hidden="true"></i> 

## Overview
[Iterable](https://iterable.com/) is a cross-channel marketing platform that powers unified customer experiences and empowers you to create, optimize and measure every interaction across the entire customer journey.

## Implementation
Currently being implemented in [this epic](https://gitlab.com/groups/gitlab-com/marketing/-/epics/3690)

## Use
Iterable will be used for the following:
   - SaaS Trial Email Nurture (Launched 2023-05-10)
   - Free User onboarding and nurture [Current flow can be found here](https://docs.google.com/presentation/d/1nb26f7NJEY-_KNkQ3GjXVCnrBCo1Y3SeW7QTBCO_DYM/edit#slide=id.p)
   - Triggered email sends off of user behavior

## Data Flows
TBC - Maintained in Snowflake by the Data Team and sent to Iterable via Hightouch and/or Workato
