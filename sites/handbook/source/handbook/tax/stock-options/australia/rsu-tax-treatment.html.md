---
layout: handbook-page-toc
title: "RSU Tax Treatment (Australia)"
---

  

## On this page

{:.no_toc .hidden-md .hidden-lg}

  

- TOC

{:toc .hidden-md .hidden-lg}

  

## DISCLAIMER

The information provided in this handbook has been designed for informational purposes only, and _**is not**_ intended to provide taxation, legal or financial advice. You should consult an accountant or tax advisor.

## Introduction

Restricted Stock Units (RSUs) are granted to team members as part of equity compensation. The Australian Tax Office (ATO) considers RSUs as **ESS interests**, which are provided by GitLab under an [**Employee share scheme (ESS)**](https://www.ato.gov.au/General/Employee-share-schemes/).

There has been some discussion over time from Australian team members in the [#loc_australia](https://gitlab.slack.com/archives/CHHFS9DR7) Slack channel about tax obligations for [Restricted Stock Units (RSUs)](https://about.gitlab.com/handbook/stock-options/#restricted-stock-units-rsus). This page aims to collect relevant material to help you determine what you _**may**_ owe at tax time. As noted in the [disclaimer](#disclaimer) above, you should consult a professional, using the information here as a starting point.
You will see ["sell to cover"](https://about.gitlab.com/handbook/stock-options/#sell-to-cover---rsus) in your RSU release confirmation, however, no shares will be sold for team members working in Australia. You should consider your tax obligations as shares are vested.

## Tax Obligations

When your RSUs vest, you become eligible to some tax obligations. There is a [useful example](https://community.ato.gov.au/s/question/a0J9s0000001HR9/p00043329) on the ATO community forum which discusses this for RSUs, but in brief, there are two main things to consider: 

- [Assessable Income](#assessable-income) (applies when shares vest)
- [Capital Gains Tax](#capital-gains-tax-cgt) (applies when shares are sold)

### Assessable Income

When your RSUs vest, the value of the shares *at that point in time* become assessable as a part of your income for that financial year. Depending on what you decide to do with the shares decides further changes: 

|Event|Action|
|--|--|
|Sell within 30 days|[Refer to the 30-Day Rule](#the-30-day-rule)|
|Sell after 30 days, but within 12 months|Value of shares are assessable income. Report a capital gain or loss|
|Sell after 12 months|Value of shares are assessable income. Report a discounted (50%) capital gain or loss|

Using an example, we can discuss a few of the above events, using the worth of 10 RSUs at $30.00 USD a share (purely as an easy-to-calculate example), for a total of $300 USD. In all cases, the cost of the RSUs need to be converted into Australian dollars based on the currency rate *on the date the ESS interests were granted*.

**If sold within 30 days**, the sold shares become assessable income at their sell price, which may have fluctuated slightly since vesting. Assuming the shares are now worth $310 USD, at tax time you would report this as income in AUD using the currency conversion from this date. You don't need to report capital gains (or loss) for this sale.

**If sold after 30 days, but within 12 months**, the shares are considered assessable income on the day they vested ($300), which you will need to consider as income on your tax return for that financial year. When you sell the RSU within 12 months, you report a full capital gain or loss based on the share price when sold. If 10 shares are now worth $330 USD, you would report a capital gain of $30. 

**If sold after 30 days, but after 12 months**, the shares are considered assessable income on the day they vested ($300), which you will need to consider as income on your tax return for that financial year. When you sell the RSU after 12 months, you report a [discounted capital gain or loss](https://www.ato.gov.au/individuals/capital-gains-tax/cgt-discount/) based on the share price when sold. If 10 shares are now worth $330 USD, you would report a capital gain of $15. 

### Capital gains tax (CGT)

A capital gain (or loss) can occur by selling or disposing of assets. In this context, RSUs are considered an asset when sold and subject to CGT for that financial year. For more detail, review [the ATO's website](https://www.ato.gov.au/Individuals/Capital-gains-tax/).

### The 30-Day Rule

The ATO has specific mention within the ESS about the [30-day rule](https://www.ato.gov.au/general/employee-share-schemes/employers/types-of-ess/concessional-ess/tax-deferred-schemes/#BK_30dayruleitcanchangethedeferredtaxing), which means, if you acquire and then dispose (sell) your acquired shares within 30 days of receiving them, the *deferred taxing point* becomes the date of the disposal (sell date). This means you have a singular tax event of the sale value of the shares, which is considered a part of your income for that financial year. This may be useful for some situations where you do not want to trigger Capital Gains.

## Record Keeping

The ATO expects you to [keep a list of records](https://www.ato.gov.au/General/Employee-share-schemes/Employees/Record-keeping/) relating to your ESS interests. Most of this information is available through the year on the Etrade platform.  It is expected that for the 22/23 financial year, detailed statements will be provided that also contain this information.

Throughout the year, you can review your stock plan confirmations in Etrade, which shows information including the Market Value, Taxable Gain and number of shares issued, presented U.S. currency. This can be found in Etrade by navigating to **Stock Plan > Quick Links > Confirmations**. Select the *Restricted Stock* documents and download the PDF(s) to view this information.

### ESS Statement and Annual Report

Under [ATO requirements](https://www.ato.gov.au/General/Employee-share-schemes/In-detail/Employer-reporting-requirements/ESS---Reporting-requirements-for-employers/), GitLab is required to send GitLab team members an ESS Statement by the 14th July after the end of that financial year. This will be beneficial to you and your tax agent (if you're engaging one) to complete your tax return and understand your obligations.

After this, GitLab must deliver an ESS Annual Report to the ATO by the 14th August, which contains information on the ESS interests provided to the employee.